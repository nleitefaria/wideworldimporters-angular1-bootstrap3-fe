wwiApp.controller('specialDealsController',['$scope', '$resource', '$http', 'NgTableParams', 'specialDealsFactory', function($scope, $resource, $http, NgTableParams, specialDealsFactory) 
{
    $scope.headingTitle = "Special Deals"; 
    
    this.tableParams = new NgTableParams({}, 
    {
      getData: function(params) 
      {      
        return $resource(apiUrl + "specialdeals/page/" + params.page()).get().$promise.then(function(data) 
        {
          params.total(data.totalElements); 
          return data.content;
        });
      }
    });
    
    
    
    
    
            
}]);

