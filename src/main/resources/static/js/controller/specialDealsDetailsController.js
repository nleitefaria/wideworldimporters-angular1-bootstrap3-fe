wwiApp.controller('specialDealsDetailsController',['$scope', '$routeParams','$resource', '$http', 'specialDealsFactory', function($scope, $routeParams, $resource, $http, specialDealsFactory) 
{
    $scope.headingTitle = "ID:"; 
    $scope.id = $routeParams.id;    
    $scope.specialDeals = specialDealsFactory.findOneSpecialDeals($routeParams.id);         
}]);

