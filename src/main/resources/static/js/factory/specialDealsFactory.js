wwiApp.factory('specialDealsFactory', [ '$resource', '$http', function($resource, $http) 
{
	var specialDealsFac = {};
	
	specialDealsFac.findOneSpecialDeals = function(id) 
	{
		return $resource("http://localhost:8085/wideworldimporters-rest-jpa-sqlserver-be/rest/specialdeals/" + id).get();
	};
	
	specialDealsFac.findOneSpecialDealsPage = function(pageNum) 
	{
		return $resource("http://localhost:8085/wideworldimporters-rest-jpa-sqlserver-be/rest/specialdeals/page/" + pageNum).get();
	};
	
	specialDealsFac.findAllSpecialDeals = function() 
	{
		return $resource("http://localhost:8085/wideworldimporters-rest-jpa-sqlserver-be/rest/specialdeals").query();
	};
	
	
	return specialDealsFac;
}])