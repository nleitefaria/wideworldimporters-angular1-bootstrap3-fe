wwiApp.factory('buyingGroupsFactory', [ '$resource', '$http', function($resource, $http) 
{
	var buyingGroupsFac = {};
	
	buyingGroupsFac.findAll = function() 
	{
		return $resource("http://localhost:8085/wideworldimporters-rest-jpa-sqlserver-be/rest/buyinggroups").query();
	};
	
	

	return buyingGroupsFac;
}])