var wwiApp = angular.module("wwiApp", [ "ngRoute", "ngTable", "ngResource" ]);

wwiApp.config(function($routeProvider) 
{
	$routeProvider
	.when("/", {
		templateUrl :'/views/home.html',
		controller: 'homeController'
	})
	.when("/buying-groups", {
		templateUrl : '/views/buying-groups/buying-groups.html',
		controller: 'buyingGroupsController'
	})
	.when("/special-deals", {
		templateUrl : '/views/special-deals/special-deals.html',
		controller: 'specialDealsController'
	})
	.when("/special-deals/:id", {
		templateUrl : '/views/special-deals/special-deals-details.html',
		controller: 'specialDealsDetailsController'
	})
	.when("/customer-categories", {
		templateUrl : '/views/customer-categories/customer-categories.html'
	})
	.when("/customers", {
		templateUrl : '/views/customers/customers.html'
	})
	.otherwise({
		redirectTo : '/views/other.html'
	});
});






////////////////

var apiUrl = "http://localhost:8085/wideworldimporters-rest-jpa-sqlserver-be/rest/";