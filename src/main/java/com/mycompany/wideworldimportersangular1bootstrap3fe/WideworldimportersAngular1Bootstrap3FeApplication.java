package com.mycompany.wideworldimportersangular1bootstrap3fe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WideworldimportersAngular1Bootstrap3FeApplication {

	public static void main(String[] args) {
		SpringApplication.run(WideworldimportersAngular1Bootstrap3FeApplication.class, args);
	}
}
